Miami area cosmetic eyelid surgeon. Evaluation and treatment for numerous types of oculoplastic conditions/issues like: blepharoplasty, ptosis, ectropion, entropion, and more. Dr. Tanenbaum is Miami’s “eye guy”. Look better, feel better. Call today to make an appointment.

Address: 7765 SW 87 Avenue, Suite 210, Miami, FL 33173

Phone: 305-273-5353
